import 'package:flutter/material.dart';

import '../../flash_country_picker.dart';

class CountryRowWidget extends StatelessWidget {
  final Country country;
  final Function(Country) onSelect;
  final CountryListThemeData? countryListTheme;
  final bool showPhoneCode;
  final TextStyle defaultTextStyle;
  const CountryRowWidget({
    super.key,
    required this.country,
    required this.onSelect,
    this.countryListTheme,
    required this.showPhoneCode,
    required this.defaultTextStyle,
  });

  @override
  Widget build(BuildContext context) {
    final TextStyle _textStyle =
        countryListTheme?.textStyle ?? defaultTextStyle;
    final bool isRtl = Directionality.of(context) == TextDirection.rtl;

    return Material(
      // Add Material Widget with transparent color
      // so the ripple effect of InkWell will show on tap
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          country.nameLocalized = CountryLocalizations.of(context)
              ?.countryName(countryCode: country.countryCode)
              ?.replaceAll(RegExp(r"\s+"), " ");
          onSelect(country);
          Navigator.pop(context);
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 6.0),
          child: Row(
            children: <Widget>[
              Row(
                children: [
                  // const SizedBox(width: 20),
                  CountryFlagWidget(
                    country: country,
                    countryListTheme: countryListTheme,
                  ),
                  if (showPhoneCode && !country.iswWorldWide) ...[
                    const SizedBox(width: 16),
                    SizedBox(
                      width: 45,
                      child: Text(
                        '${isRtl ? '' : '+'}${country.phoneCode}${isRtl ? '+' : ''}',
                        style: _textStyle,
                      ),
                    ),
                    const SizedBox(width: 6),
                  ] else
                    const SizedBox(width: 16),
                ],
              ),
              Expanded(
                child: Text(
                  CountryLocalizations.of(context)
                          ?.countryName(countryCode: country.countryCode)
                          ?.replaceAll(RegExp(r"\s+"), " ") ??
                      country.name,
                  style: _textStyle,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
