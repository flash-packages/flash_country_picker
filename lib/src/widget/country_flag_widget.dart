import 'package:flutter/material.dart';

import '../../flash_country_picker.dart';

class CountryFlagWidget extends StatelessWidget {
  final Country country;
  final CountryListThemeData? countryListTheme;
  const CountryFlagWidget({
    super.key,
    required this.country,
    this.countryListTheme,
  });

  @override
  Widget build(BuildContext context) {
    final bool isRtl = Directionality.of(context) == TextDirection.rtl;

    return SizedBox(
      // the conditional 50 prevents irregularities caused by the flags in RTL mode
      width: isRtl ? 50 : null,
      child: Text(
        country.iswWorldWide
            ? '\uD83C\uDF0D'
            : Utils.countryCodeToEmoji(country.countryCode),
        style: TextStyle(
          fontSize: countryListTheme?.flagSize ?? 24,
        ),
      ),
    );
  }
}
