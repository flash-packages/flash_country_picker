# flash_country_picker

[![pub package](https://img.shields.io/pub/v/flash_country_picker.svg)](https://pub.dev/packages/flash_country_picker)

A Flutter package for picking the country with it's flasg image and mobile number prefix.
Supports Android, iOS, Linux, macOS and Windows.
Not all methods are supported on all platforms.

|             | Android | iOS  | Linux | macOS  | Windows     |
|-------------|---------|------|-------|--------|-------------|
| **Support** | SDK 16+ | 9.0+ | Any   | 10.11+ | Windows 10+ |

## Usage

To use this plugin, add `flash_country_picker` as a [dependency in your pubspec.yaml file](https://flutter.dev/docs/development/platform-integration/platform-channels).

## Example
<?code-excerpt "readme_excerpts.dart (Example)"?>
```dart
class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) => const MaterialApp(
        home: MyScreen(),
      );
}

class MyScreen extends StatefulWidget {
  const MyScreen({super.key});

  @override
  State<MyScreen> createState() => _MyScreenState();
}

class _MyScreenState extends State<MyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flash Country Picker Example 1'),
      ),
      body: Column(
        children: const [
            // TODO
        ],
      ),
    );
  }
}

```


See this `flash_country_picker` [example](https://gitlab.com/flash-packages/flash_country_picker/-/tree/main/example/lib/main.dart) for an example.

A Codeflash's Product ✨